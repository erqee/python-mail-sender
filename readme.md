## Python Mail Sender

Tunnelling SMTP to Zoho Mail Account (whether using email@zoho.com or the custom domain one).

*Usage Example:*

```bash

shell> echo "testing email" > /tmp/my-email-text.txt
shell> python pymail-zoho.py --bot="someaccount@zoho.com" --pwd="thepassword" --recipient="email@other.com" --subject="test" --file=/tmp/my-email-text.txt

```

There's stil a lot things needs to improve.
But, for a while -- this is it.

:-)
