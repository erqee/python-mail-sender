#!/usr/bin/python

import smtplib, sys, getopt
from email.mime.text import MIMEText

def main(argv):
  bot = ''
  pwd = ''
  recipient = ''
  subject = ''
  textfile = ''

  try:
    opts, args = getopt.getopt(argv,"b:p:r:s:f",["bot=","pwd=","recipient=","subject=","file="])
  except getopt.GetoptError:
    print 'pymail -b "<bot email address>" -p "<bot email password>" -r "<recipient email>" -s "<subject>" -f <text file>'
    print 'or: pymail --bot="<bot email address>" --pwd="<bot email password>" --recipient="<recipient email>" --subject="<subject>" --file=<text file>'
    sys.exit(2)

  for opt, arg in opts:
    if opt in ("-h","--help"):
      print 'pymail -b "<bot email address>" -p "<bot email password>" -r "<recipient email>" -s "<subject>" -f <text file>'
      print 'or: pymail --bot="<bot email address>" --pwd="<bot email password>" --recipient="<recipient email>" --subject="<subject>" --file=<text file>'
      sys.exit()
    elif opt in ("-b", "--bot"):
      bot = arg
    elif opt in ("-p", "--pwd"):
      pwd = arg
    elif opt in ("-r", "--recipient"):
      recipient = arg
    elif opt in ("-s", "--subject"):
      subject = arg
    elif opt in ("-f", "--file"):
      textfile = arg

  # Create message
  fp = open(textfile)
  msg = MIMEText(fp.read())
  msg['Subject'] = subject
  msg['From'] = bot
  msg['To'] = recipient
  fp.close()

  # Create ZOHO Mail SMTP connection object with SSL option
  server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

  try:
    server.login(bot, pwd)
    server.sendmail(bot, recipient, msg.as_string())
    server.quit()
    print "Sucessfully sent email"
  except SMTPException:
    print "Error: unable to send email"

if __name__ == "__main__":
  main(sys.argv[1:])
